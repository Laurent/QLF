# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 15:56:04 2017
Update : 03-10-2017 : 14h00

@author: BOUCHET Séverin & LAURENT Pierre
"""
# Importaton of modules
import psycopg2
import argparse
import logging
import sys

#initialisation logs
from logging.handlers import RotatingFileHandler

# création de l'objet logger qui va nous servir à écrire dans les logs
logger = logging.getLogger()

# création d'un formateur qui va ajouter le temps, le niveau
# de chaque message quand on écrira un message dans le log
formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
# création d'un handler qui va rediriger une écriture du log vers
# un fichier en mode 'append', avec 1 backup et une taille max de 1Mo
file_handler = RotatingFileHandler('activity.log', 'a', 1000000, 1)
# on lui met le niveau sur DEBUG, on lui dit qu'il doit utiliser le formateur
# créé précédement et on ajoute ce handler au logger
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

# création d'un second handler qui va rediriger chaque écriture de log
# sur la console
stream_handler = logging.StreamHandler()
logger.addHandler(stream_handler)

# args for connexion to database
connect_str = "dbname='radio_libre' user='s.bouchet' host='80.82.238.198' password='P@ssword'"
# use our connection values to establish a connection
try:
    # create a psycopg2 cursor that can execute queries
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
except Exception as err:
    
    logger.critical('Unable to connect to the database')
    sys.exit(0)

# create arguments and description of this script
parser = argparse.ArgumentParser(description='Générateur de playlist')
# create argument for the duration of the playlist
parser.add_argument('-d','--duree', help='Duration of playlist', required=True)

# Log level dictionnary to help defining level inside CLI
logLevelDict = {'DEBUG' : logging.DEBUG,
                'INFO' : logging.INFO,
                'WARNING' : logging.WARNING,
                'ERROR' : logging.ERROR,
                'CRITICAL' : logging.CRITICAL}
parser.add_argument('-l','--log',
                    help='Set log level to [DEBUG], INFO, WARNING, ERROR or CRITICAL',
                    metavar='LEVEL',
                    choices=logLevelDict,
                    default='DEBUG')

# create argument for the genre of tracks and percentage
# parser.add_argument('-g','--genre', help='Genre of tracks', required=True)
def myTuple(s):
    isTuple = False
    
    for letter in s:
        if letter == ',':
            isTuple = True
    
    if isTuple:    
        try:
            x, y = map(str, s.split(','))
            return x, int(y)
        except:
            raise argparse.ArgumentTypeError("Values must be : str,[int]")
    else:
        return s, 100

parser.add_argument('--genre', type=myTuple, metavar='str,[int]',
                    help="Genre of playlist, [and percentage of this kind]",
                    action='append')
                    
parser.add_argument('--sous_genre',help='Trie par sous-genre de morceaux dans la playlist', type = str)
parser.add_argument('--album',help='Tri par nom d\'album de la playlist', type = str)
parser.add_argument('--titre',help='Tri par titre de morceaux dans la playlist', type = str)
parser.add_argument('--artiste', type=myTuple, metavar='str,[int]', help="Artist of playlist, [and percentage of this kind]", action='append')

# create argument for the name of the playlist
parser.add_argument('name', help='Name of the file who is created (Format : name.txt)')

args = parser.parse_args()

# Set log level based on CLI user parameter
logger.setLevel(logLevelDict[args.log])
file_handler.setLevel(logLevelDict[args.log])
stream_handler.setLevel(logLevelDict[args.log])

if args.genre is not None:
	# nombre total de genres
	nbGenre = len(args.genre)

	tempsEnSecondesGenre = []

	# second -> minutes
	tempsEnSecondes = int(args.duree)*60

	# time of other genre
	tempsEnSecondesReste = tempsEnSecondes

	cpt=0
	while cpt < nbGenre:   
		
		
		# time of genre
		tempsEnSecondesGenre.append(int((tempsEnSecondes*int(args.genre[cpt][1]))/100))
		
		# time of other genre
		tempsEnSecondesReste = tempsEnSecondesReste - tempsEnSecondesGenre[cpt]
		
		print(tempsEnSecondesGenre[cpt])
		
		cpt=cpt+1

	# init
	total = 0
	totalGenre = 0

	# ouverture du fichier liste
	try:
		fichier = open(args.name, "w")
	except Exception as err:
		logger.critical('Permission denied to open playlist file')
		sys.exit(0)

	cpt=0
	while cpt < nbGenre:
		i=0
		
		# Genre choisi pour l'utilisateur
		# Collect filtered tuple from database
		while i <= tempsEnSecondesGenre[cpt]:
		    try:
		        cursor.execute("SELECT * FROM morceaux WHERE genre~'%s' ORDER BY RANDOM()" % (args.genre[cpt][0]))
		        infosPiste = cursor.fetchone()
		    except Exception as err:   
		        logger.critical('Unable to fetch tuples from database')
		        sys.exit(0)
		    i = i + infosPiste[5]
		    if i<= int(tempsEnSecondesGenre[cpt]):
		        # affichage piste récupéré
		        print(infosPiste[0] + " - " + infosPiste[2] + " : " + str(infosPiste[5]) + "s" + " (" + infosPiste[3] + ")")
		        # insertion piste
		        try:
		            fichier.write(str(infosPiste[8]) + "\n")
		        except Exception as err:
		            logger.critical('Permission denied to write playlist file')
		            sys.exit(0)
		        totalGenre = totalGenre+ infosPiste[5]
		        
		cpt=cpt+1
		print("______________")

	totalReste = 0

	# autre genres aléatoire
	i=0
	while i <= tempsEnSecondesReste:
		try:
		    cursor.execute("SELECT * FROM morceaux ORDER BY RANDOM()")
		    infosPiste = cursor.fetchone()
		except Exception as err:   
		    logger.critical('Unable to fetch tuples from database')
		i = i+infosPiste[5]
		if i<= int(tempsEnSecondesReste):
		    # affichage piste récupéré
		    print(infosPiste[0] + " - " + infosPiste[2] + " : " + str(infosPiste[5]) + "s" + " (" + infosPiste[3] + ")")
		    # insertion piste
		    try:
		        fichier.write(str(infosPiste[8]) + "\n")
		    except Exception as err:
		        logger.critical('Permission denied to write playlist file')
		        sys.exit(0)
		    totalReste = totalReste + infosPiste[5]

	total = totalGenre+totalReste

	# print the list of the playlist and duration
	print("\n\nDurée totale de la playlist : " + str(total) + " secondes (" + str(args.duree) + " minutes)")

	# calcul the time whitout song to put waiting sound
	blanc = tempsEnSecondes - total
	print("Il y aura " + str(blanc) + " secondes de musique d'ambiance et " + str(args.genre[0][1]) + "% de " + str(args.genre[0][0]))

	# fermeture du fichier
	try:
		fichier.close()
	except Exception as err:
		logger.critical('Unable to close playlist file')
		sys.exit(0)

elif args.artiste is not None:
	# nombre total d'artiste
	nbArtiste = len(args.artiste)

	tempsEnSecondesArtiste = []

	# second -> minutes
	tempsEnSecondes = int(args.duree)*60

	# time of other artiste
	tempsEnSecondesReste = tempsEnSecondes

	cpt=0
	while cpt < nbArtiste:   
	
		# time of genre
		tempsEnSecondesArtiste.append(int((tempsEnSecondes*int(args.artiste[cpt][1]))/100))
	
		# time of other genre
		tempsEnSecondesReste = tempsEnSecondesReste - tempsEnSecondesArtiste[cpt]
	
		cpt=cpt+1

	# init
	total = 0
	totalArtiste = 0

	# ouverture du fichier liste
	try:
		fichier = open(args.name, "w")
	except Exception as err:
		logger.critical('Permission denied to open playlist file')
		sys.exit(0)

	cpt=0
	while cpt < nbArtiste:
		i=0
		
		# Genre choisi pour l'utilisateur
		# Collect filtered tuple from database
		while i <= tempsEnSecondesArtiste[cpt]:
		    try:
		        cursor.execute("SELECT * FROM morceaux WHERE artiste~'%s' ORDER BY RANDOM()" % (args.artiste[cpt][0]))
		        infosPiste = cursor.fetchone()
		    except Exception as err:   
		        logger.critical('Unable to fetch tuples from database')
		        sys.exit(0)
		    i = i + infosPiste[5]
		    if i<= int(tempsEnSecondesArtiste[cpt]):
		        # affichage piste récupéré
		        print(infosPiste[0] + " - " + infosPiste[2] + " : " + str(infosPiste[5]) + "s" + " (" + infosPiste[3] + ")")
		        # insertion piste
		        try:
		            fichier.write(str(infosPiste[8]) + "\n")
		        except Exception as err:
		            logger.critical('Permission denied to write playlist file')
		            sys.exit(0)
		        totalArtiste = totalArtiste+ infosPiste[5]
		        
		cpt=cpt+1
		print("______________")

	totalReste = 0

	# autres artistes aléatoires
	i=0
	while i <= tempsEnSecondesReste:
		try:
		    cursor.execute("SELECT * FROM morceaux ORDER BY RANDOM()")
		    infosPiste = cursor.fetchone()
		except Exception as err:   
		    logger.critical('Unable to fetch tuples from database')
		i = i+infosPiste[5]
		if i<= int(tempsEnSecondesReste):
		    # affichage piste récupéré
		    print(infosPiste[0] + " - " + infosPiste[2] + " : " + str(infosPiste[5]) + "s" + " (" + infosPiste[3] + ")")
		    # insertion piste
		    try:
		        fichier.write(str(infosPiste[8]) + "\n")
		    except Exception as err:
		        logger.critical('Permission denied to write playlist file')
		        sys.exit(0)
		    totalReste = totalReste + infosPiste[5]

	total = totalArtiste+totalReste

	# print the list of the playlist and duration
	print("\n\nDurée totale de la playlist : " + str(total) + " secondes (" + str(args.duree) + " minutes)")

	# calcul the time whitout song to put waiting sound
	blanc = tempsEnSecondes - total
	print("Il y aura " + str(blanc) + " secondes de musique d'ambiance et " + str(args.artiste[0][1]) + "% de " + str(args.artiste[0][0]))

	# fermeture du fichier
	try:
		fichier.close()
	except Exception as err:
		logger.critical('Unable to close playlist file')
		sys.exit(0)
