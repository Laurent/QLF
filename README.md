# Getting Started
This software is a tool who allows your to generate playlist files for multimedia player.
It's allow you to create and generate random playlist with many op-
tions like :
  - duration of playlist
  - gender of track
  - artist of track
  - title of track
  - name of generated file

# Prerequisites
This playlist generator require [Python](https://www.python.org/) version >= 2.7

# Installation
You have to write this command to install this software :
```
pip3 install generateur-1.0.tar.gz
```

# Running
To execute this software, you must have to type :
```
generateur-playlist –duree=60 playlist.txt
gplaylist –duree=60 playlist.txt
```

```
generateur-playlist –duree=60 --genre="[Rr]ock",30 --artiste="Pink Floyd",60 playlist.txt
```

# Versioning
Version  1.0
December 5, 2017

# Authors
Pierre LAURENT and Séverin BOUCHET

# License
[![N|Solid](https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png)]()